import { Navigation } from 'react-native-navigation';
import registerScreens from './registerScreen';
import { Colors } from '@theme/Colors';
import { Fonts } from '@theme/Fonts';
import { createStore } from 'redux';
import { Provider } from 'react-redux';
import reducers from '../reducers';
export const store = createStore(reducers);

registerScreens(store, Provider);

export default class App {
  constructor() {
    Navigation.events().registerAppLaunchedListener(() => {
      Navigation.setDefaultOptions({
        bottomTab: {
          iconColor: Colors.inactiveTabColor,
          selectedIconColor: Colors.activeTabColor,
          textColor: Colors.inactiveTabColor,
          selectedTextColor: Colors.activeTabColor,
          fontSize: 13,
          fontFamily: Fonts.SemiboldFont
        },
        topBar: {
          visible: false,
          drawBehind: true,
        },
        statusBar: {
          style: 'light'
        },
        bottomTabs: {
          titleDisplayMode: "alwaysShow",
          animate: true,
          backgroundColor: Colors.tabBarBacgroundColor
        },
        layout: {
          orientation: ["portrait"],
          direction: 'ltr'
        },
      });
      Navigation.setRoot({
        root: {
          bottomTabs: {
            children: [{
              stack: {
                children: [{
                  component: {
                    name: "app.Home",
                    options: {
                      topBar: {
                        visible: false,
                        drawBehind: true,
                      }
                    }
                  }
                }],
                options: {
                  bottomTab: {
                    text: 'Home',
                    iconInsets: { top: 0, left: 0, right: 0, bottom: 0 },
                    icon: require('@assets/Images/Tabs/home.png'),
                    testID: 'FIRST_TAB_BAR_BUTTON'
                  }
                }
              }
            },
            {
              stack: {
                children: [{
                  component: {
                    name: "app.Sectors",
                    options: {
                      topBar: {
                        visible: false,
                        drawBehind: true,
                      }
                    }
                  }
                }],
                options: {
                  bottomTab: {
                    text: 'Sector-wise',
                    iconInsets: { top: 0, left: 0, right: 0, bottom: 0 },
                    icon: require('@assets/Images/Tabs/dashboard.png'),
                    testID: 'SECOND_TAB_BAR_BUTTON'
                  }
                }
              }
            },
            {
              stack: {
                children: [{
                  component: {
                    name: "app.More",
                    options: {
                      topBar: {
                        visible: false,
                        drawBehind: true,
                      }
                    }
                  }
                }],
                options: {
                  bottomTab: {
                    text: 'More',
                    iconInsets: { top: 0, left: 0, right: 0, bottom: 0 },
                    icon: require('@assets/Images/Tabs/more.png'),
                    testID: 'SECOND_TAB_BAR_BUTTON'
                  }
                }
              }
            }
            ]
          }
        }
      });
    });
  }
}
