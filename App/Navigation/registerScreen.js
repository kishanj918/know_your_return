import { Navigation } from 'react-native-navigation';

// imports the screens
import Home from '@screens/Home';
import Result from '@screens/Result';
import Sectors from '@screens/Sectors';
import StockList from '@screens/StockList';
import TopList from '@screens/TopList';
import More from '@screens/More';
import PrivacyPolicy from '@screens/PrivacyPolicy';
import Disclaimer from '@screens/Disclaimer';
import Strategy from '@screens/Strategy';
import ContactUs from '@screens/ContactUs';

// register the screens
const registerScreens = (store, Provider) => {
  Navigation.registerComponentWithRedux('app.Home', () => Home, Provider, store);
  Navigation.registerComponentWithRedux('app.Result', () => Result, Provider, store);
  Navigation.registerComponentWithRedux('app.Sectors', () => Sectors, Provider, store);
  Navigation.registerComponentWithRedux('app.StockList', () => StockList, Provider, store);
  Navigation.registerComponent('app.TopList', () => TopList);
  Navigation.registerComponent('app.More', () => More);
  Navigation.registerComponent('app.PrivacyPolicy', () => PrivacyPolicy);
  Navigation.registerComponent('app.Disclaimer', () => Disclaimer);
  Navigation.registerComponent('app.Strategy', () => Strategy);
  Navigation.registerComponent('app.ContactUs', () => ContactUs);
}

export default registerScreens