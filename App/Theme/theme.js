import {
  StyleSheet,
  Dimensions,
} from 'react-native';
import { Colors } from '@theme/Colors';
import { Fonts } from '@theme/Fonts';

const deviceWidth = Dimensions.get('window').width;

export default StyleSheet.create({
  body: {
    flex: 1,
    paddingHorizontal: 20,
  },
  com: {
    color: '#63aaf2',
    fontSize: 18,
    fontFamily: Fonts.SemiboldFont,
    paddingBottom: 10,
  },
  noTopMargin: {
    elevation: 3,
    borderRadius: 5,
    shadowOffset: { width: 1, height: 1 },
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOpacity: 0.3,
    padding: 10,
  },
  investContainer: {
    elevation: 3,
    borderRadius: 5,
    shadowOffset: { width: 1, height: 1 },
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOpacity: 0.3,
    padding: 10,
    marginTop: 30,
  },
  DDInputStyle: {
    paddingHorizontal: 10
  },
  durationCon: {
    width: deviceWidth - 65,
    padding: 10,
  },
  serachData: {
    height: 150,
    position: 'absolute',
    top: 30,
    backgroundColor: '#fff',
    borderWidth: 1,
    borderColor: 'gray',
  },
  renderSearchRow: {
    width: deviceWidth - 60,
    backgroundColor: '#fff',
    padding: 10,
    zIndex: 3000,
  },
})