export const Colors = {
  textColor: '#63aaf2',
  backColor: '#ffffff',
  bottomBorder: '#E8E8E8',
  searchBackColor: '#ffff',
  searchfontColor: '#000',

  headerColors: ['#1B9CFC', '#1B9CFC'],
  btnColor: '#53C68C',
  activeTabColor: 'rgba(255,255,255,1)',
  inactiveTabColor: 'rgba(255,255,255,0.5)',
  tabBarBacgroundColor: '#636e72',

  segmentedTabActiveColor: '#1B9CFC',
  segmentedTabInactiveColor: '#B6B6CA',
  segmentedTabActiveTextColor: '#ffffff',
  segmentedTabInactiveTextColor: '#808080',

  listPageBackgroundColor: '#FDFDFD', //  all listing page background

  toggleButtonOn: '#4CAF50',
  toggleButtonOff: '#F44336',

  progressLoss: '#F44336',
  progressProfit: '#4CAF50',

  categoryTitle: '#B22222',

  progressIncrease: ['#4CAF50', '#4CAF50'],
  progressDecrease: ['#F44336', '#F44336'],

  viewMoreBtn: '#1B9CFC',
  indicatorColor: '#1B9CFC'
}