export const Fonts = {
  BoldFont: 'OpenSans-Bold',
  SemiboldFont: 'OpenSans-SemiBold',
  LightFont: 'OpenSans-Light',
  RegularFont: 'OpenSans-Regular',
}