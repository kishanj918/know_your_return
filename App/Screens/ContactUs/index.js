import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  Platform,
  TouchableOpacity,
  Dimensions,
  Image,
} from 'react-native';
import Header from '@component/Header';
import { Fonts } from '@theme/Fonts';
import { Colors } from '@theme/Colors';
import Communications from 'react-native-communications';

const { width } = Dimensions.get('window');
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.listPageBackgroundColor,
  },
  scrollViewContainer: {
    paddingTop: 10,
    paddingHorizontal: 15,
  },
  textStyle: {
    fontSize: 16,
    fontFamily: Fonts.RegularFont,
    color: '#000',
    flexWrap: 'wrap',
    textAlign: 'justify',
    marginVertical: 5,
  },
});

export default class ContactUs extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { componentId } = this.props;
    return (
      <View style={styles.container}>
        <Header
          componentId={componentId}
          showBack
          title="Contact Us"
        />
        <ScrollView contentContainerStyle={styles.scrollViewContainer}>
          <Image
            source={require('@assets/Images/app_icon.png')}
            style={{ height: width * 0.67, width: width * 0.67, resizeMode: 'contain', alignSelf: 'center' }}
          />
          <Text style={styles.textStyle}>
            For any inquiry or app development you can contact us on the below email
          </Text>
          <Text style={styles.textStyle}>
            <Text style={{ fontFamily: Fonts.BoldFont }}>Email: </Text>
            <Text
              onPress={() => {
                Communications.email(
                  Platform.OS == 'ios' ? ['kishanjobanputra4@gmail.com'] : ['9stacksoft@gmail.com'],
                  null,
                  null,
                  null,
                  null
                )
              }}
            >
              {
                Platform.OS == 'ios' ?
                  'kishanjobanputra4@gmail.com'
                  :
                  '9stacksoft@gmail.com'
              }
            </Text>
          </Text>
        </ScrollView>
      </View>
    );
  }
}