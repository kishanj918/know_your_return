import React from 'react';
import {
  Text,
  View,
  Dimensions,
  ScrollView,
  FlatList,
  ActivityIndicator,
  Platform,
} from 'react-native';
import { connect } from 'react-redux';
import SegmentedControlTab from "react-native-segmented-control-tab";
import axios from 'axios';
import firebase from 'react-native-firebase';
import ProgressCard from '@component/ProgressCard';
import { BASE_URL, timeDurationData } from '@config/config';
import { Colors } from '@theme/Colors';
import Header from '@component/Header';
import styles from './style';

class StockList extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      selectedIndex: 0,
      collpaseSelectedIndex: null,
      listArray: [],
      showSpinner: true,
      exchangeType: ["BSE", "NSE"],
    }
    this._renderStyle = this._renderStyle.bind(this);
    this._renderItem = this._renderItem.bind(this);
  }

  componentDidMount() {
    const { sector, time_frame, isLowest } = this.props.selectedSector;
    const { exchangeType, selectedIndex } = this.state;
    const { searchSectorAdCount } = this.props.data;
    const requestData = {
      sector_id: sector.id,
      time_frame: time_frame,
      flag: isLowest,
      exchange: exchangeType[selectedIndex]
    };
    this.getSectorList(requestData);
    if (searchSectorAdCount >= 2) {
      if (searchSectorAdCount == 2) {
        this.__showAdvertisement();
      } else {
        const num = searchSectorAdCount - 2;
        if (num % 3 == 0) {
          this.__showAdvertisement();
        }
      }
    }
  }

  __showAdvertisement = () => {
    const unitId = Platform.OS == 'ios' ?
      'ca-app-pub-4740773905539482/5358346386' :
      'ca-app-pub-4740773905539482/2320409069';
    const advert = firebase.admob().interstitial(unitId);
    const AdRequest = firebase.admob.AdRequest;
    const request = new AdRequest();
    request.addKeyword('foo').addKeyword('bar');

    // Load the advert with our AdRequest
    advert.loadAd(request.build());


    advert.on('onAdLoaded', () => {
      if (__DEV__) {
        console.log('Advert ready to show.');
      }
    });

    advert.on('onAdFailedToLoad', (abc) => {
      if (__DEV__) {
        console.log(abc);
      }
    })

    // Simulate the interstitial being shown "sometime" later during the apps lifecycle
    setTimeout(() => {
      if (advert.isLoaded()) {
        advert.show();
      } else {
        // Unable to show interstitial - not loaded yet.
      }
    }, Platform.OS === 'ios' ? 2000 : 3000);
  }

  getSectorList(requestData) {
    axios.post(`${BASE_URL}sectorwisesotock`, requestData)
      .then((response) => {
        this.setState({
          showSpinner: false,
          listArray: response.data ? response.data : [],
        });
      })
      .catch((error) => {
        if (__DEV__) {
          console.log(error);
        }
        this.setState({
          showSpinner: false,
          listArray: [],
        });
      });
  }

  handleIndexChange = index => {
    this.setState({
      ...this.state,
      selectedIndex: index,
      showSpinner: true,
      listArray: []
    }, () => {
      const { sector, time_frame, isLowest } = this.props.selectedSector;
      const { exchangeType } = this.state;
      const requestData = {
        sector_id: sector.id,
        time_frame: time_frame,
        flag: isLowest,
        exchange: exchangeType[index].toLowerCase()
      };
      this.getSectorList(requestData);
    });
  };

  _renderStyle(value) {
    const style = value.indexOf('-') == -1 ?
      [styles.singleArea, { color: 'green' }]
      :
      [styles.singleArea, { color: 'red' }]
    return style
  }

  changeCollapsedValue(index) {
    this.setState({
      collpaseSelectedIndex: index
    })
  }

  _keyExtractor = (item, index) => item + index;

  _renderItem = ({ item }) => {
    const { time_frame } = this.props.selectedSector;
    return (
      <ProgressCard
        data={item}
        timeFrame={time_frame}
      />
    );
  }


  render() {
    const { componentId, selectedSector } = this.props;
    const { selectedIndex, listArray, exchangeType, showSpinner } = this.state;
    const { isLowest, selectedDuration, sector } = selectedSector
    return (
      <View style={styles.container}>
        <Header
          componentId={componentId}
          showBack
          title={isLowest ? `Max Gainers in ${timeDurationData[selectedDuration].value}` : `Worst Losers in ${timeDurationData[selectedDuration].value}`}
        />
        <View style={{ marginHorizontal: 10 }}>
          <SegmentedControlTab
            tabTextStyle={styles.segmentTabTextStyle}
            activeTabTextStyle={styles.segmentActiveTabTextStyle}
            values={exchangeType}
            selectedIndex={selectedIndex}
            tabStyle={styles.tabStyle}
            activeTabStyle={styles.activeTabStyle}
            onTabPress={this.handleIndexChange}
          />
        </View>
        <View style={styles.sectorNameView}>
          <Text style={styles.sectorName}>Sector: {sector.name}</Text>
        </View>
        {
          showSpinner ? (
            <View style={[styles.container, { justifyContent: 'center', alignItems: 'center' }]}>
              <ActivityIndicator size="large" color={Colors.indicatorColor} />
            </View>
          ) : listArray.length ? (
            <FlatList
              data={listArray}
              contentContainerStyle={{ paddingTop: 20 }}
              keyExtractor={this._keyExtractor}
              renderItem={this._renderItem}
            />
          ) : (
                <View style={[styles.container, { justifyContent: 'center', alignItems: 'center' }]}>
                  <Text style={[styles.sectorTypeText, { color: 'red' }]}>No Data Found</Text>
                </View>
              )
        }
      </View>
    )
  }
}

const mapStateToProps = (state) => {
  return {
    data: state.dataReducer
  };
}

export default connect(mapStateToProps)(StockList);