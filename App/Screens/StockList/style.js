import {
  StyleSheet,
  Dimensions,
  Platform,
} from 'react-native';
import { Colors } from '@theme/Colors';
import { Fonts } from '@theme/Fonts';

const deviceWidth = Dimensions.get('window').width;

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.listPageBackgroundColor,
  },
  cardView: {
    marginHorizontal: 10,
    padding: 20,
    backgroundColor: '#fff',
    borderRadius: 12,
    marginVertical: 8,
  },
  singleArea: {
    width: deviceWidth / 3 - 40,
  },
  headingText: {
    fontSize: 20,
    fontWeight: 'bold',
    marginBottom: 10,
  },
  sectorTypeText: {
    fontSize: 18,
    fontWeight: '500',
  },
  shimmerStyle: {
    height: 200,
    width: deviceWidth,
  },
  overlay: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: 'rgba(69,85,117,0.4)',
  },

  toggleView: {
    marginTop: 20,
    backgroundColor: '#fff',
    borderRadius: 12,

    padding: 10,
    //borderWidth: 1,
    // backgroundColor: 'gray',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  switch: {
    padding: 4,
    backgroundColor: '#000',
    justifyContent: 'center',
    width: 60,
    borderRadius: 15,
    height: 30
  },
  filterTxt: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#000',
  },
  tabStyle: {
    marginTop: 5,
    height: 40,
    borderColor: '#696969',
    borderWidth: StyleSheet.hairlineWidth,
    color: '#fff',
  },
  activeTabStyle: {
    backgroundColor: Colors.segmentedTabActiveColor,
    borderColor: Colors.segmentedTabActiveColor,
    borderWidth: StyleSheet.hairlineWidth,
  },
  segmentTabTextStyle: {
    color: Colors.segmentedTabInactiveTextColor,
    fontFamily: Fonts.SemiboldFont,
    fontSize: 15,
  },
  segmentActiveTabTextStyle: {
    color: Colors.segmentedTabActiveTextColor,
    fontFamily: Fonts.SemiboldFont,
    fontSize: 15,
  },
  sectorNameView: {
    marginHorizontal: 15,
    flexWrap: 'wrap',
    marginTop: 15
  },
  sectorName: {
    color: '#000',
    fontFamily: Fonts.SemiboldFont,
    fontSize: 15,
  },
})