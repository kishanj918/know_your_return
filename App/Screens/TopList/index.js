import React, { Component } from 'react';
import {
  View,
  Text,
  FlatList,
} from 'react-native';
import { Navigation } from 'react-native-navigation';
import { timeDurationData } from '@config/config';
import Header from '@component/Header';
import ProgressCard from '@component/ProgressCard';
import styles from './style';
import { Fonts } from '../../Theme/Fonts';

export default class TopList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      bestData: this.props.bestList,
      timeFrame: this.props.timeFrame,
      exchangeType: this.props.exchangeType,
      selectedDuration: this.props.selectedDuration,
      note: this.props.note
    };
    this._renderItem = this._renderItem.bind(this);
  }

  goBack() {
    Navigation.pop(this.props.componentId);
    return true;
  }

  _keyExrator = (item, index) => index.toString();

  _renderItem = ({ item, index }) => {
    const { selectedDuration, timeFrame, exchangeType } = this.state;
    // return (
    //   <View style={styles.detailContainer}>
    //     <View style={styles.detailContainerLeftSide}>
    //       <Text numberOfLines={2} style={styles.nameLabelStyle}>{item.company_name}</Text>
    //       <Text numberOfLines={2} style={styles.timeFrameLabelStyle}>Time Frame:  {timeDurationData[selectedDuration].value}</Text>
    //       <Text numberOfLines={2} style={styles.timeFrameLabelStyle}>Exchange: {exchangeType}</Text>
    //     </View>
    //     <View
    //       style={[
    //         styles.detailContainerRightSide,
    //         { backgroundColor: Number(item[`${timeFrame}`]) <= 0 ? 'red' : 'green' },
    //       ]}>
    //       {
    //         Number(item[`${timeFrame}`]) <= 0 ? (
    //           <Text style={styles.arrowStyle}>
    //             &#8595;
    //           </Text>
    //         ) : (
    //             <Text style={styles.arrowStyle}>
    //               &#8593;
    //             </Text>
    //           )
    //       }
    //       <Text style={styles.glLabel}>{item[`${timeFrame}`]}%</Text>
    //     </View>
    //   </View>
    // );
    const { timeFrame: time_frame } = this.props;
    return (
      <ProgressCard
        data={item}
        timeFrame={time_frame}
      />
    );
  }

  render() {
    const { bestData, note } = this.state;
    const { componentId, selectedDuration } = this.props;
    return (
      <View style={styles.container}>
        <Header
          componentId={componentId}
          showBack
          title={`Top Gainers in ${timeDurationData[selectedDuration].value}`} />
        <Text style={styles.noteStyle}>
          <Text style={{ fontFamily: Fonts.BoldFont }}>Note: </Text>
          <Text>
            {note}
          </Text>
        </Text>

        <FlatList
          data={bestData}
          contentContainerStyle={{ paddingTop: 30 }}
          keyExtractor={this._keyExrator}
          renderItem={this._renderItem}
        />
      </View>
    );
  }
}