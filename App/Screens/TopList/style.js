import { StyleSheet, Platform } from 'react-native';
import { Colors } from '@theme/Colors';
import { Fonts } from '@theme/Fonts';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.listPageBackgroundColor,
  },
  detailContainer: {
    backgroundColor: '#fff',
    marginBottom: 10,
    marginHorizontal: 10,
    paddingLeft: 10,
    paddingRight: 5,
    height: 80,
    borderRadius: 7,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  detailContainerLeftSide: {
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  detailContainerRightSide: {
    height: '90%',
    width: '25%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 7
  },
  nameLabelStyle: {
    fontSize: 18,
    color: '#000',
    fontFamily: Fonts.RegularFont
  },
  timeFrameLabelStyle: {
    fontSize: 15,
    color: '#000',
    fontFamily: Fonts.RegularFont
  },
  arrowStyle: {
    fontSize: Platform.OS == 'ios' ? 15 : 20,
    color: '#fff',
    fontFamily: Fonts.BoldFont,
    marginRight: 5,
    marginTop: Platform.OS == 'ios' ? 0 : -5,
  },
  glLabel: {
    fontSize: 15,
    color: '#fff',
    fontFamily: Fonts.BoldFont
  },
  noteStyle: {
    fontSize: 16,
    color: '#000',
    fontFamily: Fonts.RegularFont,
    marginTop: 10,
    marginHorizontal: 15,
  }
});
