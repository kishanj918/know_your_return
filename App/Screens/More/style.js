import {
  StyleSheet,
  Dimensions,
} from 'react-native';
import { Colors } from '@theme/Colors';
import { Fonts } from '@theme/Fonts';

const { width } = Dimensions.get('window');

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.listPageBackgroundColor,
  },
  borderStyle: {
    borderBottomColor: '#676767',
    borderBottomWidth: StyleSheet.hairlineWidth,
  },
  menuButton: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 50,
    marginHorizontal: 20,
  },
  menuText: {
    fontSize: 17,
    fontFamily: Fonts.RegularFont,
    color: '#4a4a4a',
  },
  meuIcon: {
    height: 20,
    width: 20,
    marginRight: 20,
  },
});
