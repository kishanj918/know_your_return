import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ScrollView, Image, Share, Platform } from 'react-native';
import { Navigation } from 'react-native-navigation';
import Header from '@component/Header';
import styles from './style';

export default class More extends Component {
	constructor(props) {
		super(props);
		this.state = {};
	}

	onShare() {
		const message =
			'Stock Screener Plus : Find The Max Return Giving Stock.' +
			'\n\nFor Android : ' +
			'https://play.google.com/store/apps/details?id=com.stock.screener.plus' +
			'\n\nFor iOS  : ' +
			'https://apps.apple.com/in/app/stock-screener-plus/id1470407449';
		Share.share({
			message: message,
			title: 'Share This App',
		});
	}

	navigateTo(screen, passedProps) {
		Navigation.push(this.props.componentId, {
			component: {
				name: screen,
				passProps: passedProps,
				options: {
					bottomTabs: {
						visible: false,
						drawBehind: true,
					},
				},
			},
		});
	}

	render() {
		return (
			<View style={styles.container}>
				<Header title="More" showBack={false} componentId={this.props.componentId} />
				<ScrollView style={{ flexGrow: 1 }}>
					<TouchableOpacity onPress={() => this.navigateTo('app.Strategy')} style={styles.menuButton}>
						<Image source={require('@assets/Images/More/strategies.png')} style={styles.meuIcon} />
						<Text style={styles.menuText}>Strategies</Text>
					</TouchableOpacity>
					<View style={[styles.borderStyle]} />
					<TouchableOpacity onPress={() => this.onShare()} style={styles.menuButton}>
						<Image source={require('@assets/Images/More/share.png')} style={styles.meuIcon} />
						<Text style={styles.menuText}>Share This App</Text>
					</TouchableOpacity>
					<View style={[styles.borderStyle]} />
					<TouchableOpacity onPress={() => this.navigateTo('app.ContactUs')} style={styles.menuButton}>
						<Image source={require('@assets/Images/More/phone-receiver.png')} style={styles.meuIcon} />
						<Text style={styles.menuText}>Contact Us</Text>
					</TouchableOpacity>
					<View style={[styles.borderStyle]} />
					<TouchableOpacity style={styles.menuButton} onPress={() => this.navigateTo('app.PrivacyPolicy')}>
						<Image source={require('@assets/Images/More/shield-with-lock.png')} style={styles.meuIcon} />
						<Text style={styles.menuText}>Privacy Policy</Text>
					</TouchableOpacity>
					<View style={[styles.borderStyle]} />
					<TouchableOpacity onPress={() => this.navigateTo('app.Disclaimer')} style={styles.menuButton}>
						<Image source={require('@assets/Images/More/disclaimer.png')} style={styles.meuIcon} />
						<Text style={styles.menuText}>Disclaimer</Text>
					</TouchableOpacity>
					<View style={[styles.borderStyle]} />
				</ScrollView>
			</View>
		);
	}
}
