import React from 'react';
import {
  Text,
  View,
  Image,
  FlatList,
  ActivityIndicator,
  TouchableOpacity,
  TextInput,
  TouchableWithoutFeedback,
  Modal,
  Platform
} from 'react-native';
import firebase from 'react-native-firebase';
import { connect } from 'react-redux';
import { Navigation } from 'react-native-navigation';
import { Colors } from '@theme/Colors';
import ShimmerPlaceHolder from 'react-native-shimmer-placeholder';
import axios from 'axios';
import { timeDurationData, BASE_URL } from '@config/config';
import AlertComponent from '@component/AlertComponent';
import DurationListComponent from '@component/DurationListComponent';
import styles from './style';
import * as dataActions from '../../actions/dataActions';
import { getNetConnectionInfo } from '../../Services/functions';

// customeComponents
import Header from '@component/Header';

const Banner = firebase.admob.Banner;
const AdRequest = firebase.admob.AdRequest;
const request = new AdRequest();
const unitId = Platform.OS == 'ios' ?
  'ca-app-pub-4740773905539482/8670194991' :
  'ca-app-pub-4740773905539482/2181464807';

class Sectors extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      data: undefined,
      search: '',
      searchArray: undefined,
      isFetched: true,
      isHighestReturn: true,
      selectedSector: undefined,
      invstAmt: undefined,
      selectedDuration: undefined,
      durationData: timeDurationData,
      showError: false,
      errorTitle: '',
      errorMessage: '',
      showDurationList: false,
      selectedDurationText: '',
      showSearchSectorModal: false,
    }
    this.getSectors = this.getSectors.bind(this);
    this._renderSearchArray = this._renderSearchArray.bind(this);
    this.tempData = [];
  }

  async componentDidMount() {
    let netInfo = await getNetConnectionInfo();
    if (netInfo.type !== 'wifi' && netInfo.type !== 'cellular') {
      this.setState({
        showError: true,
        errorTitle: 'Network Error',
        errorMessage: 'Please check your network connection',
      });
    } else {
      this.getSectors();
    }
  }

  getSectors() {
    axios.get(`${BASE_URL}getsectors`)
      .then((response) => {
        const data = response.data;
        this.tempData = data;
        this.setState({
          data,
        })
      })
      .catch((error) => {
        if (__DEV__) {
          console.log(error);
        }
      });
  }

  onCloseErrorBox() {
    this.setState({
      showError: false,
      errorMessage: '',
      errorTitle: '',
    });
  }

  _handleNavigation = (screenName, selectedSector) => {
    Navigation.push(this.props.componentId, {
      component: {
        name: screenName,
        passProps: {
          text: 'Pushed screen',
          selectedSector
        },
        options: {
          bottomTabs: {
            visible: false,
            drawBehind: true,
          }
        }
      }
    });
  }

  _keyExtractor = (item, index) => item + index;

  _renderItem = ({ item }) => {
    return (
      <TouchableOpacity
        style={[styles.sectorStripe]}
        onPress={() => {
          this.setState({ selectedSector: item, search: item.name })
        }}
      >
        <Text style={[styles.sectorTypeText]} numberOfLines={2} >{item.name}</Text>
      </TouchableOpacity>
    )
  }

  async navigateToSectorDetails() {
    const { selectedDuration, isHighestReturn, selectedSector, selectedDurationText } = this.state;
    const { updateSectorShowAdCount } = this.props;
    let netInfo = await getNetConnectionInfo();
    if (netInfo.type !== 'wifi' && netInfo.type !== 'cellular') {
      this.setState({
        showError: true,
        errorTitle: 'Network Error',
        errorMessage: 'Please check your network connection',
      });
    } else if (!selectedSector) {
      this.setState({
        showError: true,
        errorMessage: 'Please select any sector.',
        errorTitle: 'Note',
      });
    } else if (isNaN(selectedDuration)) {
      this.setState({
        showError: true,
        errorMessage: 'Please select time period.',
        errorTitle: 'Note',
      });
    } else {
      let sectorData = {
        sector: selectedSector,
        time_frame: timeDurationData[selectedDuration].name + '_Change',
        isLowest: isHighestReturn,
        selectedDuration: selectedDuration
      }
      updateSectorShowAdCount();
      firebase.analytics()
        .logEvent(
          'search_sector',
          {
            sectorName: selectedSector.name,
            sectorUserType: isHighestReturn ? 'gainers' : 'losers',
            timeFrame: timeDurationData[selectedDuration].name + '_Change'
          }
        );
      this._handleNavigation('app.StockList', sectorData);
    }
  }

  _renderShimmersItem = () => {
    return (
      <ShimmerPlaceHolder style={[styles.shimmerStyle]} autoRun={true} />
    )
  }

  _renderSearchArray(text) {
    this.setState({
      search: text,
    });
    if (this.tempData.length <= 0) {
      this.getSectors();
    } else {
      let newArr = this.tempData.filter((item) => {
        let temp = item.name.toLowerCase();
        if (temp.includes(text.toLowerCase())) {
          return item
        }
      });
      this.setState({
        data: text ? newArr : this.tempData,
        // showList: text && newArr.length ? true : false,
      })
    }
  }

  hanldeSwitch = () => {
    this.setState({
      isHighestReturn: !this.state.isHighestReturn
    })
  }

  clearSearch() {
    this.setState({
      search: '',
      showList: false,
      selectedSector: undefined,
    });
  }

  handleTimePeriod(data, i) {
    this.setState({
      showDurationList: false,
      selectedDuration: i,
      selectedDurationText: data.value
    });
  }

  onCloseModel() {
    this.setState({
      showDurationList: false
    });
  }

  render() {
    const { componentId } = this.props;
    const {
      data,
      search,
      isHighestReturn,
      errorMessage,
      errorTitle,
      showError,
      showDurationList,
      selectedDurationText,
      showSearchSectorModal,
    } = this.state;

    return (
      <View style={styles.container}>
        <Header
          componentId={componentId}
          showBack={false}
          title='Sector List'
        />
        <View style={{ flex: 1, paddingHorizontal: 20 }}>
          <View style={styles.timeframeContainer}>
            <TouchableOpacity
              onPress={() => this.setState({ showSearchSectorModal: true })}
              style={styles.timeframeButton}
            >
              <Text style={styles.timeframeButtonText}>{search ? search : 'Search Sector (Eg. Banks, Pharma)'}</Text>
            </TouchableOpacity>
          </View>
          <View style={styles.toggleView}>
            <Text style={[styles.filterTxt, { color: isHighestReturn ? Colors.toggleButtonOn : Colors.toggleButtonOff }]}>{isHighestReturn ? 'Gainers' : 'Losers'}</Text>
            <TouchableWithoutFeedback onPress={this.hanldeSwitch}>
              <View style={[styles.switch, { backgroundColor: isHighestReturn ? Colors.toggleButtonOn : Colors.toggleButtonOff }]}>
                <View style={[styles.switchBtn, { alignSelf: isHighestReturn ? 'flex-start' : 'flex-end' }]} />
              </View>
            </TouchableWithoutFeedback>
          </View>
          <View style={styles.timeframeContainer}>
            <TouchableOpacity
              onPress={() => this.setState({ showDurationList: true })}
              style={styles.timeframeButton}
            >
              <Text style={styles.timeframeButtonText}>{selectedDurationText ? selectedDurationText : 'Time Period'}</Text>
            </TouchableOpacity>
          </View>
          <TouchableOpacity onPress={() => this.navigateToSectorDetails()} style={styles.btn}>
            <Text style={styles.btnText}> Calculate </Text>
          </TouchableOpacity>
          <Text style={styles.noteText}>
            <Text style={styles.boldFont}>Note: </Text>
            <Text style={styles.regularFont}>You can check sector-wise which stocks delivered max and min return in a selected time period like 1 week, 1 month, etc.</Text>
          </Text>
          <AlertComponent
            showModal={showError}
            title={errorTitle}
            description={errorMessage}
            onClose={() => this.onCloseErrorBox()}
          />
          <DurationListComponent
            showModal={showDurationList}
            handleTimePeriod={(data, i) => this.handleTimePeriod(data, i)}
            onClose={() => this.onCloseModel()}
          />
          <Modal
            visible={showSearchSectorModal}
            animated
            animationType="slide"
            onDismiss={() => this.setState({ showSearchSectorModal: false })}
          >
            <View style={styles.container}>
              <View style={styles.modalHeader} />
              <TouchableOpacity
                onPress={() => this.setState({ showSearchSectorModal: false })}
                style={styles.modalCloseButton}>
                <Image
                  source={require('@assets/Images/backArrow.png')}
                  style={styles.modalCloseIcon}
                />
              </TouchableOpacity>
              <View style={styles.searchBar}>
                <TextInput
                  ref={inp => this.searchBar = inp}
                  style={styles.searchInputStyle}
                  underlineColorAndroid="transparent"
                  placeholder="Search Sector (Eg. Banks, Pharma)"
                  placeholderTextColor="#000"
                  value={search}
                  onChangeText={(txt) => this._renderSearchArray(txt)}
                />
                {
                  search
                    ? (
                      <TouchableOpacity
                        onPress={() => this.setState({ search: '', data: this.tempData, selectedSector: undefined })}
                        style={styles.clearButton}>
                        <Image
                          source={require('@assets/Images/error.png')}
                          style={styles.clearIcon}
                        />
                      </TouchableOpacity>
                    ) : (
                      <TouchableOpacity
                        onPress={() => this.searchBar.focus()}
                        style={styles.clearButton}
                      >
                        <Image
                          source={require('@assets/Images/search.png')}
                          style={styles.clearIcon}
                        />
                      </TouchableOpacity>
                    )
                }
              </View>
              <FlatList
                style={styles.listStyle}
                contentContainerStyle={{ paddingBottom: 20 }}
                data={data}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item }) => {
                  return (
                    <TouchableOpacity
                      activeOpacity={0.9}
                      onPress={() => {
                        this.setState({
                          selectedSector: item,
                          search: item.name,
                          showSearchSectorModal: false,
                          data: this.tempData,
                        });
                      }}
                      style={[styles.renderSearchRow]}
                    >
                      <Text style={styles.listLabelStyle}>
                        {item.name}
                      </Text>
                    </TouchableOpacity>
                  )
                }}
              />
            </View>
          </Modal>
        </View>
        <Banner
          unitId={unitId}
          size={"SMART_BANNER"}
          request={request.build()}
          onAdLoaded={() => { }}
          onAdFailedToLoad={(abc) => { }}
        />
      </View>
    )
  }
}

function mapStateToProps(state) {
  return {
    data: state.dataReducer
  };
}

const mapDispatchToProps = (dispatch) => {
  return {
    updateSectorShowAdCount: () => dispatch(dataActions.updateSectorShowAdCount()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Sectors);
