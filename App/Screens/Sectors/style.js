import {
  StyleSheet,
  Dimensions,
  Platform
} from 'react-native';
import { ifIphoneX } from 'react-native-iphone-x-helper';
import { Fonts } from '@theme/Fonts';
import { Colors } from '@theme/Colors';

const { height, width } = Dimensions.get('window');

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.listPageBackgroundColor,
  },
  sectorTypeText: {
    marginHorizontal: 5,
    fontSize: 18,
    fontFamily: Fonts.SemiboldFont,
  },
  icon: {
    height: 20,
    width: 20,
    tintColor: '#000'
  },
  iconView: {
    position: 'absolute',
    top: 18,
    right: 25,
  },
  searchView: {
    marginVertical: 10,
    height: 40,
    backgroundColor: '#eaeff1',
    borderRadius: 5,
    padding: 10,
    color: '#000',
    fontFamily: Fonts.RegularFont,
  },
  sectorStripe: {
    paddingHorizontal: 10,
    // marginVertical: 5,
    height: 60,
    justifyContent: 'center',
    borderBottomColor: '#63aaf2',
    backgroundColor: '#f6f6f6',
    borderBottomWidth: StyleSheet.hairlineWidth,
    // borderRadius: 8,
  },
  shimmerStyle: {
    height: 40,
    alignSelf: 'center',
    width: width - 20,
    marginVertical: 8,
  },
  toggleView: {
    marginTop: 20,
    borderRadius: 12,
    zIndex: 100,
    paddingVertical: 10,
    //borderWidth: 1,
    // backgroundColor: 'gray',
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  switch: {
    padding: 4,
    justifyContent: 'center',
    width: 60,
    borderRadius: 15,
    height: 30
  },
  filterTxt: {
    fontSize: 20,
    fontFamily: Fonts.BoldFont,
  },
  investAmountContainer: {
    borderRadius: 5,
    shadowColor: '#000',
    borderBottomWidth: 1,
    borderColor: '#999999',
    marginTop: 20,
    alignSelf: 'center',
    width: '95%',
    // marginHorizontal: 15,
  },
  floatingTxt: {
    fontSize: 14,
    color: '#000',
    padding: 5,
  },
  investInput: {
    fontSize: 18,
    paddingBottom: 15,
    paddingTop: 10,
    color: '#999999',
    paddingHorizontal: 5,
  },
  timeframeContainer: {
    borderRadius: 5,
    borderColor: '#999999',
    borderBottomWidth: 1,
    paddingVertical: 15,
    marginTop: 10,
    width: '100%',
    alignSelf: 'center'
  },
  listViewStyle: {
    height: 250,
    width: '100%',
    position: 'absolute',
    top: height * 0.05,
    zIndex: 1000,
    alignSelf: 'center'
  },
  btn: {
    width: '100%',
    backgroundColor: Colors.btnColor,
    marginTop: 60,
    borderRadius: 4,
    marginBottom: 10,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'center',
  },
  btnText: {
    fontSize: 18,
    color: '#fff',
    padding: 15,
    fontFamily: Fonts.SemiboldFont,
  },
  timeframeButton: {
    width: '100%',
    height: 30,
    paddingTop: Platform.OS == 'ios' ? 8 : 5,
  },
  timeframeButtonText: {
    fontSize: 18,
    color: '#000000',
    paddingLeft: 5,
    fontFamily: Fonts.RegularFont,
  },
  noteText: {
    fontSize: 15,
    color: '#000',
    textAlign: 'justify',
  },
  boldFont: {
    fontFamily: Fonts.BoldFont
  },
  regularFont: {
    fontFamily: Fonts.RegularFont
  },
  switchBtn: {
    height: 24,
    width: 24,
    borderRadius: 12,
    backgroundColor: '#fff'
  },
  modalHeader: {
    ...ifIphoneX({
      paddingTop: 50
    }, {
        paddingTop: Platform.OS == 'ios' ? 35 : 20
      }),
    paddingHorizontal: 20,
    paddingBottom: 25,
    backgroundColor: Colors.segmentedTabActiveColor
  },
  modalCloseButton: {
    position: 'absolute',
    ...ifIphoneX({
      top: 35,
    }, {
        top: Platform.OS == 'ios' ? 25 : 8
      }),
    left: 5,
  },
  modalCloseIcon: {
    height: 25,
    width: 25,
    tintColor: '#fff',
    resizeMode: 'contain'
  },
  searchBar: {
    height: 40,
    width: '95%',
    backgroundColor: 'rgba(0,0,0,0.2)',
    marginTop: 10,
    alignSelf: 'center',
    paddingHorizontal: 10,
    borderRadius: 7,
  },
  searchInputStyle: {
    color: '#000',
    minHeight: 40,
    width: '90%',
  },
  listStyle: {
    alignSelf: 'center',
    width: '100%',
    paddingHorizontal: 10,
    paddingTop: 5,
  },
  listLabelStyle: {
    fontFamily: Fonts.BoldFont,
    fontSize: 18,
    color: '#000',
  },
  clearButton: {
    position: 'absolute',
    right: 10,
    top: '25%',
  },
  clearIcon: {
    height: 20,
    width: 20,
    resizeMode: 'stretch',
    tintColor: '#000000'
  },
  timeframeContainer: {
    borderRadius: 5,
    borderColor: '#999999',
    borderBottomWidth: 1,
    paddingVertical: 15,
    marginTop: 10,
  },
  iconStyle: {
    position: 'absolute',
    left: 10,
    top: '25%',
    height: 20,
    width: 20,
    resizeMode: 'stretch',
    tintColor: '#000000'
  },
  timeframeButton: {
    width: '100%',
    paddingHorizontal: 5,
    minHeight: 30,
    paddingTop: 6,
  },
  timeframeButtonText: {
    fontSize: 18,
    color: '#000000',
    fontFamily: Fonts.RegularFont,
  },
  renderSearchRow: {
    width: '100%',
    // borderWidth: 1,
    borderBottomWidth: 1,
    borderColor: '#E8E8E8',
    backgroundColor: '#fff',
    padding: 10,
    zIndex: 3000,
  },
})