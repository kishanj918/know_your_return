import React from 'react';
import {
  View,
  Dimensions,
  ActivityIndicator,
  FlatList,
  Platform
} from 'react-native';
import { connect } from 'react-redux';
import { Navigation } from 'react-native-navigation';
import axios from 'axios';
import firebase from 'react-native-firebase';
import Header from '@component/Header';
import { BASE_URL, timeDurationData } from '@config/config';
import { Colors } from '@theme/Colors';
import BarComponent from './barListView';
import styles from './style';

class Result extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      durationData: timeDurationData,
      fetchData: true,
      dataList: [],
      allData: undefined,
    };
    this.navigateToTopList = this.navigateToTopList.bind(this);
  }

  componentDidMount() {
    const { knowYourReturnAdCount } = this.props.dataReducer;
    const { exchangeType, data, selectedDuration, resultAmount, invstAmt } = this.props;
    const { durationData } = this.state;
    let timeFrame = `${durationData[selectedDuration].name}_Change`;
    let requestData = {
      exchange: exchangeType,
      stock_id: data.id,
      time_frame: timeFrame
    };
    axios.post(`${BASE_URL}bestreturnstock`, requestData)
      .then(res => {
        let bestReturnStockData = res.data;
        const selectedStock = {
          company_name: data.company_name,
          price: Number(resultAmount).toFixed(2),
          change: data[timeFrame],
          barWidth: this.calculateBarWidth(Number(data[timeFrame])),
          title: `Your Return in ${durationData[selectedDuration].value}`,
          // category: '',
        };
        const lessthanfifty = bestReturnStockData.lessthanfifty.length && {
          company_name: bestReturnStockData.lessthanfifty[0].company_name,
          price: Number(parseFloat(invstAmt) + this.calculatePLAmount(bestReturnStockData.lessthanfifty[0])).toFixed(2),
          change: bestReturnStockData.lessthanfifty[0][`${durationData[selectedDuration].name}_Change`],
          barWidth: this.calculateBarWidth(Number(bestReturnStockData.lessthanfifty[0][`${durationData[selectedDuration].name}_Change`])),
          title: `Max Returns in ${durationData[selectedDuration].value}:`,
          category: 'Stock < 50 ₹',
          more: 'View more',
          fullList: bestReturnStockData.lessthanfifty,
          topListNote: 'This list includes only stock that has value between 0-50 ₹.'
        };
        const fiftytohundred = bestReturnStockData.fiftytohundred.length && {
          company_name: bestReturnStockData.fiftytohundred[0].company_name,
          price: Number(parseFloat(invstAmt) + this.calculatePLAmount(bestReturnStockData.fiftytohundred[0])).toFixed(2),
          change: bestReturnStockData.fiftytohundred[0][`${durationData[selectedDuration].name}_Change`],
          barWidth: this.calculateBarWidth(Number(bestReturnStockData.fiftytohundred[0][`${durationData[selectedDuration].name}_Change`])),
          // title: 'from stock price between 51 to 100'
          category: 'Stock 51-100 ₹',
          more: 'View more',
          fullList: bestReturnStockData.fiftytohundred,
          topListNote: 'This list includes only stock that has value between 51-100 ₹.'
        };
        const hundredtofivehundred = bestReturnStockData.hundredtofivehundred.length && {
          company_name: bestReturnStockData.hundredtofivehundred[0].company_name,
          price: Number(parseFloat(invstAmt) + this.calculatePLAmount(bestReturnStockData.hundredtofivehundred[0])).toFixed(2),
          change: bestReturnStockData.hundredtofivehundred[0][`${durationData[selectedDuration].name}_Change`],
          barWidth: this.calculateBarWidth(Number(bestReturnStockData.hundredtofivehundred[0][`${durationData[selectedDuration].name}_Change`])),
          // title: 'Highest return from stock price between 101 to 500'
          category: 'Stock 101-500 ₹',
          more: 'View more',
          fullList: bestReturnStockData.hundredtofivehundred,
          topListNote: 'This list includes only stock that has value between 101-500 ₹.'
        };
        const grtthanfivehundred = bestReturnStockData.grtthanfivehundred.length && {
          company_name: bestReturnStockData.grtthanfivehundred[0].company_name,
          price: Number(parseFloat(invstAmt) + this.calculatePLAmount(bestReturnStockData.grtthanfivehundred[0])).toFixed(2),
          change: bestReturnStockData.grtthanfivehundred[0][`${durationData[selectedDuration].name}_Change`],
          barWidth: this.calculateBarWidth(Number(bestReturnStockData.grtthanfivehundred[0][`${durationData[selectedDuration].name}_Change`])),
          // title: 'Highest return from stock price 500+'
          category: 'Stock 500 > ₹',
          more: 'View more',
          fullList: bestReturnStockData.grtthanfivehundred,
          topListNote: 'This list includes only stock that has value 500+ ₹.'
        };
        let listArr = [selectedStock, lessthanfifty, fiftytohundred, hundredtofivehundred, grtthanfivehundred];
        listArr = listArr.filter(i => i);
        this.setState({
          fetchData: false,
          dataList: listArr,
          allData: bestReturnStockData,
        });
      })
      .catch(err => {
        if (__DEV__) {
          console.log('err', err.response);
        }
        this.setState({
          fetchData: false,
        });
      });
    if (knowYourReturnAdCount >= 2) {
      if (knowYourReturnAdCount == 2) {
        this.__showAdvertisement();
      } else {
        const num = knowYourReturnAdCount - 2;
        if (num % 3 == 0) {
          this.__showAdvertisement();
        }
      }
    }
  }

  __showAdvertisement = () => {
    const unitId = Platform.OS == 'ios' ?
      'ca-app-pub-4740773905539482/9268960791' :
      'ca-app-pub-4740773905539482/3522466632';
    const advert = firebase.admob().interstitial(unitId);
    const AdRequest = firebase.admob.AdRequest;
    const request = new AdRequest();
    request.addKeyword('foo').addKeyword('bar');

    // Load the advert with our AdRequest
    advert.loadAd(request.build());


    advert.on('onAdLoaded', () => {
      if (__DEV__) {
        console.log('Advert ready to show.');
      }
    });

    advert.on('onAdFailedToLoad', (abc) => {
      if (__DEV__) {
        console.log(abc);
      }
    })

    // Simulate the interstitial being shown "sometime" later during the apps lifecycle
    setTimeout(() => {
      if (advert.isLoaded()) {
        advert.show();
      } else {
        // Unable to show interstitial - not loaded yet.
      }
    }, Platform.OS === 'ios' ? 2000 : 3000);
  }

  calculateBarWidth(per) {
    let val = Math.abs(per);
    let percentage = parseFloat((100 * val) / 100).toFixed(2);
    if (percentage > 100) {
      percentage = parseFloat(100).toFixed(2);
    }
    let width = Dimensions.get('window').width - 60;
    const countWidth = (parseFloat(width).toFixed(2) * percentage) / 100;
    return countWidth;
  }

  calculatePLAmount(passedData) {
    const { durationData } = this.state;
    const { invstAmt, selectedDuration } = this.props;
    const timeFrame = durationData[selectedDuration].name;
    const selected_Change = `${timeFrame}_Change`;
    const p_l_amount = (parseFloat(invstAmt) * parseFloat(passedData[`${selected_Change}`])) / 100;
    return p_l_amount;
  }

  navigateToTopList(dataList) {
    const { exchangeType, selectedDuration, componentId } = this.props;
    const { durationData } = this.state;
    let timeFrame = `${durationData[selectedDuration].name}_Change`;
    Navigation.push(componentId, {
      component: {
        name: 'app.TopList',
        passProps: {
          exchangeType,
          timeFrame,
          bestList: dataList,
          selectedDuration,
        },
        options: {
          bottomTabs: {
            visible: false,
            drawBehind: true,
          }
        }
      }
    });
  }

  render() {
    const { componentId, selectedDuration, exchangeType } = this.props;
    const { fetchData, dataList } = this.state;
    return (
      <View style={styles.mainContainer}>
        <Header
          componentId={componentId}
          // showReturn
          // returnValue={resultAmount}
          showBack
          title='Your Investment Return' />
        {
          fetchData ? (
            <View style={[styles.mainContainer, styles.centerStyle]}>
              <ActivityIndicator
                size="large"
                color={Colors.indicatorColor}
              />
            </View>
          ) : (
              <FlatList
                data={dataList}
                indicatorStyle="white"
                contentContainerStyle={{ paddingHorizontal: 10, paddingBottom: 30 }}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({ item, index }) => {
                  return (
                    <BarComponent
                      data={item}
                      componentId={componentId}
                      selectedDuration={selectedDuration}
                      exchangeType={exchangeType}
                      index={index}
                    />
                  );
                }}
              />
            )
        }
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    dataReducer: state.dataReducer
  };
}

export default connect(mapStateToProps)(Result);
