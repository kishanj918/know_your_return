import {
  StyleSheet,
  Dimensions,
  Platform
} from 'react-native';
import { Colors } from '@theme/Colors';
import { Fonts } from '@theme/Fonts';

const { width, height } = Dimensions.get('window');

export default StyleSheet.create({
  mainContainer: {
    flex: 1,
    backgroundColor: Colors.listPageBackgroundColor,
  },
  centerStyle: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  backgroundImage: {
    flex: 1,
  },
  invstValue: {
    textAlign: 'center',
    textAlignVertical: 'center',
    paddingBottom: 0,
  },
  btnContainer: {
    paddingHorizontal: 20,
    //  flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  container: {
    width: "100%",
    height: 40,
    padding: 3,
    borderColor: "#FAA",
    borderWidth: 3,
    borderRadius: 30,
    marginTop: 200,
    justifyContent: "center",
  },
  inner: {
    width: "100%",
    height: 30,
    borderRadius: 15,
    backgroundColor: "green",
  },
  label: {
    fontSize: 23,
    color: "black",
    position: "absolute",
    zIndex: 1,
    alignSelf: "center",
  },
  companyLabel: {
    paddingBottom: 10,
    fontSize: 15,
    fontFamily: Fonts.RegularFont,
    color: '#000',
    paddingHorizontal: 15,
  },
  barStyle: {
    width: width - 60,
    backgroundColor: '#bfbfbf',
    borderRadius: ((width - 60) + 27) / 2,
    overflow: 'hidden',
    marginHorizontal: 15,
    marginBottom: 5
  },
  barListContainer: {
    marginVertical: 10,
    ...Platform.select({
      android: {
        elevation: 5,
      }
    }),
  },
  barListCardView: {
    backgroundColor: '#fff',
    paddingVertical: 7.5,
    // borderWidth: StyleSheet.hairlineWidth,
    borderRadius: 5,
    shadowColor: '#000',
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.2,
    shadowRadius: 2,
    elevation: 5,
    zIndex: 0,
  },
  moreButton: {
    position: 'absolute',
    top: 7,
    right: 10,
    zIndex: 1000,
  },
  perStyle: {
    color: '#fff',
    zIndex: 10000,
    position: 'absolute',
    left: 0,
    right: 0,
    textAlign: 'center',
    marginTop: '1%',
    paddingBottom: 0,
  },
  rowStyle: {
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
  },
  titleStyle: {
    paddingHorizontal: 0,
    paddingTop: 20,
    paddingBottom: 30,
    fontSize: 20,
  },
  categoryLabelStyle: {
    color: '#636e72',
    paddingBottom: 7,
    fontFamily: Fonts.SemiboldFont,
    fontSize: 17,
  },
  borderStyle: {
    width: width - 20,
    height: StyleSheet.hairlineWidth,
    backgroundColor: 'rgba(0,0,0,0.5)',
  },
  companyNameStyle: {
    marginTop: 15,
    fontFamily: Fonts.SemiboldFont,
    fontSize: 18,
  },
  gainLossLabelStyle: {
    marginTop: 15,
    fontFamily: Fonts.SemiboldFont,
    fontSize: 18,
  },
  staticLeftLabelStyle: {
    marginTop: 5,
    paddingBottom: 0,
    fontSize: 18,
    paddingHorizontal: 0,
  },
  displayPriceStyle: {
    paddingBottom: 10,
    fontSize: 18,
    fontFamily: Fonts.BoldFont,
    paddingHorizontal: 0,
  }
})