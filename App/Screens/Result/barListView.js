import React, { Component } from 'react';
import {
  View,
  Text,
  Animated,
  TouchableOpacity,
} from 'react-native';
import { Navigation } from 'react-native-navigation';
import { timeDurationData } from '@config/config';
import { Colors } from '@theme/Colors';
import styles from './style';

export default class BarListView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      animated: new Animated.Value(0),
      durationData: [...timeDurationData],
    };
  }

  componentDidMount() {
    const { barWidth } = this.props.data;
    Animated.timing(this.state.animated, {
      toValue: barWidth,
      duration: 1000
    }).start()
  }

  navigateToTopList(dataList) {
    const { exchangeType, selectedDuration, componentId, data } = this.props;
    const { durationData } = this.state;
    let timeFrame = `${durationData[selectedDuration].name}_Change`;
    Navigation.push(componentId, {
      component: {
        name: 'app.TopList',
        passProps: {
          exchangeType,
          timeFrame,
          bestList: dataList,
          selectedDuration,
          note: data.topListNote
        },
        options: {
          bottomTabs: {
            visible: false,
            drawBehind: true,
          }
        }
      }
    });
  }

  render() {
    const { data } = this.props;
    const displayPrice = Number(data.price).toFixed(2).replace(/(\d)(?=(\d{2})+\d\.)/g, '$1,');
    const profitLossColor = Number(data.change) <= 0 ? Colors.progressLoss : Colors.progressProfit;
    return (
      <View style={styles.barListContainer}>
        {data.title && <Text style={[styles.companyLabel, styles.titleStyle]}>{data.title}</Text>}
        <View style={styles.barListCardView}>
          {
            data.more && (
              <TouchableOpacity
                onPress={() => this.navigateToTopList(data.fullList)}
                style={styles.moreButton}>
                <Text style={[styles.companyLabel, { color: Colors.viewMoreBtn }]}>{data.more}</Text>
              </TouchableOpacity>
            )
          }
          {data.category && <Text style={[styles.companyLabel, styles.categoryLabelStyle]}>{data.category}</Text>}
          {data.category && <View style={styles.borderStyle} />}
          <View style={styles.rowStyle}>
            <Text style={[styles.companyLabel, styles.companyNameStyle]}>{data.company_name}</Text>
          </View>
          {/* {
            !data.hideBar && ( */}
          <View style={styles.barStyle}>
            <Animated.View
              style={{
                height: 27,
                width: this.state.animated,
                backgroundColor: profitLossColor,
                zIndex: 0,
              }}
            />
            <Text style={[styles.companyLabel, styles.perStyle]}>{data.change}%</Text>
          </View>
          {/* )
          } */}
          <View style={{ alignItems: 'center', justifyContent: 'center' }}>
            <Text style={[styles.companyLabel, styles.staticLeftLabelStyle]}>You would have made </Text>
            <Text style={[styles.companyLabel, styles.displayPriceStyle, { color: profitLossColor }]}>{displayPrice} ₹</Text>
          </View>
        </View>
      </View>
    );
  }
}
