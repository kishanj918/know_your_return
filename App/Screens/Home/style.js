import {
  StyleSheet,
  Dimensions,
  Platform
} from 'react-native';
import { ifIphoneX } from 'react-native-iphone-x-helper';
import { Colors } from '@theme/Colors';
import { Fonts } from '@theme/Fonts';

const { width } = Dimensions.get('window');

export default StyleSheet.create({
  container: {
    flex: 1,
    zIndex: 1000,
    backgroundColor: Colors.listPageBackgroundColor,
  },
  btn: {
    width: width - 40,
    backgroundColor: Colors.btnColor,
    marginTop: 40,
    borderRadius: 4,
    marginBottom: 20,
  },
  btnText: {
    fontSize: 18,
    padding: 15,
    color: '#fff',
    textAlign: 'center',
    fontFamily: Fonts.SemiboldFont,
  },
  renderSearchRow: {
    width: '100%',
    // borderWidth: 1,
    borderBottomWidth: 1,
    borderColor: '#E8E8E8',
    backgroundColor: '#fff',
    padding: 10,
    zIndex: 3000,
  },
  selectStockContainer: {
    borderRadius: 5,
    borderBottomWidth: 1,
    borderColor: '#999999',
    marginTop: 20,
  },
  investAmountContainer: {
    borderRadius: 5,
    shadowColor: '#000',
    borderBottomWidth: 1,
    borderColor: '#999999',
    marginTop: 20,
  },
  timeframeContainer: {
    borderRadius: 5,
    borderColor: '#999999',
    borderBottomWidth: 1,
    paddingVertical: 15,
    marginTop: 10,
  },
  investInput: {
    fontSize: 18,
    paddingBottom: 15,
    paddingTop: 10,
    paddingHorizontal: 15,
    color: '#000000',
    width: '87%',
    marginLeft: '7%',
    fontFamily: Fonts.RegularFont
  },
  clearButton: {
    position: 'absolute',
    right: 10,
    top: '25%',
  },
  clearIcon: {
    height: 20,
    width: 20,
    resizeMode: 'stretch',
    tintColor: '#000000'
  },
  iconStyle: {
    position: 'absolute',
    left: 10,
    top: '25%',
    height: 20,
    width: 20,
    resizeMode: 'stretch',
    tintColor: '#000000'
  },
  tabStyle: {
    marginTop: 5,
    height: 40,
    borderColor: Colors.segmentedTabInactiveColor,
    borderWidth: 1,
    color: '#fff',
  },
  activeTabStyle: {
    backgroundColor: Colors.segmentedTabActiveColor,
    borderColor: Colors.segmentedTabActiveColor,
    borderWidth: 1,
  },
  topLabel: {
    textAlign: 'center',
    fontSize: 20,
    fontFamily: Fonts.BoldFont,
    color: '#000',
    paddingTop: 5,
  },
  listViewStyle: {
    height: 190,
    position: 'absolute',
    top: 115,
    marginHorizontal: 20,
  },
  listScrollViewStyle: {
    width: width - 45,
    alignSelf: 'center',
  },
  timeframeButton: {
    width: '100%',
    paddingLeft: '11%',
    height: 30,
    paddingTop: 6,
  },
  timeframeButtonText: {
    fontSize: 18,
    color: '#000000',
    fontFamily: Fonts.RegularFont,
  },
  segmentTabTextStyle: {
    color: Colors.segmentedTabInactiveTextColor,
    fontFamily: Fonts.SemiboldFont,
    fontSize: 15,
  },
  segmentActiveTabTextStyle: {
    color: Colors.segmentedTabActiveTextColor,
    fontFamily: Fonts.SemiboldFont,
    fontSize: 15,
  },
  noteText: {
    fontSize: 15,
    color: '#000',
    textAlign: 'justify',
  },
  boldFont: {
    fontFamily: Fonts.BoldFont
  },
  regularFont: {
    fontFamily: Fonts.RegularFont
  },
  modalCloseButton: {
    position: 'absolute',
    ...ifIphoneX({
      top: 35,
    }, {
        top: Platform.OS == 'ios' ? 25 : 8
      }),
    left: 5,
  },
  modalCloseIcon: {
    height: 25,
    width: 25,
    tintColor: '#fff',
    resizeMode: 'contain',
  },
  searchBar: {
    height: 40,
    width: '95%',
    backgroundColor: 'rgba(0,0,0,0.2)',
    marginTop: 10,
    alignSelf: 'center',
    paddingHorizontal: 10,
    borderRadius: 7,
  },
  searchInputStyle: {
    color: '#000',
    minHeight: 40,
    width: '90%',
  },
  listStyle: {
    alignSelf: 'center',
    width: '100%',
    paddingHorizontal: 10,
    paddingTop: 5,
  },
  listLabelStyle: {
    fontFamily: Fonts.BoldFont,
    fontSize: 18,
    color: '#000',
  },
  modalHeader: {
    ...ifIphoneX({
      paddingTop: 50
    }, {
        paddingTop: Platform.OS == 'ios' ? 35 : 20
      }),
    paddingHorizontal: 20,
    paddingBottom: 25,
    backgroundColor: Colors.segmentedTabActiveColor
  },
})