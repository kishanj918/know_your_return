import React, { Component } from 'react';
import {
	View,
	Text,
	TextInput,
	TouchableOpacity,
	Image,
	Keyboard,
	Modal,
	FlatList,
	Platform,
	ScrollView,
} from 'react-native';
import SegmentedControlTab from 'react-native-segmented-control-tab';
import firebase from 'react-native-firebase';
import { Navigation } from 'react-native-navigation';
import { connect } from 'react-redux';
import SplashScreen from 'react-native-splash-screen';
import axios from 'axios';
import Header from '@component/Header';
import AlertComponent from '@component/AlertComponent';
import DurationListComponent from '@component/DurationListComponent';
import { BASE_URL, timeDurationData } from '@config/config';
import styles from './style';
import { getNetConnectionInfo } from '../../Services/functions';
import * as dataActions from '../../actions/dataActions';

class Home extends Component {
	constructor(props) {
		super(props);
		this.state = {
			selectedIndex: 0,
			durationData: timeDurationData,
			stockData: [],
			stockValue: '',
			invstAmt: '',
			passedData: undefined,
			selectedDuration: undefined,
			exchangeType: ['BSE', 'NSE'],
			showError: false,
			errorTitle: '',
			errorMessage: '',
			showDurationList: false,
			selectedDurationText: '',
			showSearchStockModal: false,
			showAd: true,
		};
		this.handleInvestement = this.handleInvestement.bind(this);
		this._keyboardDidHide = this._keyboardDidHide.bind(this);
		this._keyboardDidShow = this._keyboardDidShow.bind(this);
		firebase.admob().initialize('ca-app-pub-3940256099942544~3347511713');
	}

	async componentDidMount() {
		let netInfo = await getNetConnectionInfo();
		if (netInfo.type !== 'wifi' && netInfo.type !== 'cellular') {
			this.setState({
				showError: true,
				errorTitle: 'Network Error',
				errorMessage: 'Please check your network connection',
			});
		}
		if (Platform.OS == 'android') {
			SplashScreen.hide();
		}
		this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
		this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
	}

	componentWillUnmount() {
		this.keyboardDidShowListener.remove();
		this.keyboardDidHideListener.remove();
	}

	_keyboardDidShow() {
		Navigation.mergeOptions(this.props.componentId, {
			bottomTabs: {
				visible: false,
				drawBehind: true,
			},
		});
		this.setState({
			showAd: false,
		});
	}

	_keyboardDidHide() {
		Navigation.mergeOptions(this.props.componentId, {
			bottomTabs: {
				visible: true,
				drawBehind: false,
			},
		});
		this.setState({
			showAd: true,
		});
	}

	handleInvestement(text) {
		this.setState({
			invstAmt: text,
		});
	}

	calculatePLAmount(passedData) {
		const { durationData, invstAmt, selectedDuration } = this.state;
		const timeFrame = durationData[selectedDuration].name;
		const selected_Change = `${timeFrame}_Change`;
		const p_l_amount = (parseFloat(invstAmt) * parseFloat(passedData[`${selected_Change}`])) / 100;
		return p_l_amount;
	}

	handleCalculate = async () => {
		const { passedData, invstAmt, selectedIndex, exchangeType, selectedDuration } = this.state;
		const { updateStockShowAdCount } = this.props;
		let netInfo = await getNetConnectionInfo();
		if (netInfo.type !== 'wifi' && netInfo.type !== 'cellular') {
			this.setState({
				showError: true,
				errorTitle: 'Network Error',
				errorMessage: 'Please check your network connection',
			});
		} else if (!passedData) {
			this.setState({
				showError: true,
				errorMessage: 'Please select any stock.',
				errorTitle: 'Note',
			});
		} else if (!Number(invstAmt) > 0) {
			this.setState({
				showError: true,
				errorMessage: 'Please enter investment amount.',
				errorTitle: 'Note',
			});
		} else if (isNaN(selectedDuration)) {
			this.setState({
				showError: true,
				errorMessage: 'Please select time period.',
				errorTitle: 'Note',
			});
		} else {
			const p_l_amount = this.calculatePLAmount(passedData);
			firebase.analytics().logEvent('search_stock_company', {
				companyName: passedData.company_name,
				investmentAmount: invstAmt,
				timeFrame: timeDurationData[selectedDuration].name + '_Change',
			});
			updateStockShowAdCount();
			if (!isNaN(p_l_amount)) {
				const finalAmount =
					p_l_amount > 0 ? parseFloat(invstAmt) + p_l_amount : invstAmt - Math.abs(p_l_amount);
				Navigation.push(this.props.componentId, {
					component: {
						name: 'app.Result',
						passProps: {
							data: passedData,
							resultAmount: finalAmount,
							exchangeType: exchangeType[selectedIndex],
							selectedDuration: selectedDuration,
							invstAmt: invstAmt,
						},
						options: {
							bottomTabs: {
								visible: false,
								drawBehind: true,
							},
						},
					},
				});
			} else {
				this.setState({
					showError: true,
					errorTitle: 'Error',
					errorMessage: `Data doesn’t exist for ${timeDurationData[selectedDuration].value}`,
				});
			}
		}
	};

	handleStoc(text) {
		const { selectedIndex, exchangeType } = this.state;
		this.setState({ stockValue: text });
		let sendingData = {};
		if (text && text.length > 1) {
			sendingData['searchstring'] = text;
			sendingData['exchange'] = exchangeType[selectedIndex];
			axios
				.post(`${BASE_URL}findallstock`, sendingData)
				.then(data => {
					if (data.data && data.data.length) {
						this.setState({
							stockData: text ? data.data : [],
						});
					} else {
						this.setState({
							stockData: [],
						});
					}
				})
				.catch(err => {
					if (__DEV__) {
						console.log('search error => ', err);
					}
				});
		}
	}

	handleIndexChange = index => {
		this.setState({
			selectedIndex: index,
		});
	};

	handleStockInput() {
		this.setState({
			stockValue: '',
			passedData: undefined,
		});
	}

	onCloseErrorBox() {
		this.setState({
			showError: false,
			errorMessage: '',
			errorTitle: '',
		});
	}

	onCloseModel() {
		this.setState({
			showDurationList: false,
		});
	}

	handleTimePeriod(data, i) {
		this.setState({
			showDurationList: false,
			selectedDuration: i,
			selectedDurationText: data.value,
		});
	}

	handleInvestementAmount() {
		this.setState({
			invstAmt: '',
		});
	}

	render() {
		const {
			invstAmt,
			stockData,
			stockValue,
			selectedIndex,
			exchangeType,
			errorMessage,
			errorTitle,
			showError,
			showDurationList,
			selectedDurationText,
			showSearchStockModal,
			showAd,
		} = this.state;

		const Banner = firebase.admob.Banner;
		const AdRequest = firebase.admob.AdRequest;
		const request = new AdRequest();
		const unitId =
			Platform.OS == 'ios' ? 'ca-app-pub-4740773905539482/1690510211' : 'ca-app-pub-4740773905539482/5382343218';
		return (
			<View style={styles.container}>
				<Header title="If you had Invested" showBack={false} componentId={this.props.componentId} />
				<Text style={styles.topLabel}>In</Text>
				<View style={{ flex: 1 }}>
					<ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{ paddingHorizontal: 20 }}>
						<SegmentedControlTab
							tabTextStyle={styles.segmentTabTextStyle}
							activeTabTextStyle={styles.segmentActiveTabTextStyle}
							values={exchangeType}
							tabStyle={styles.tabStyle}
							selectedIndex={selectedIndex}
							activeTabStyle={styles.activeTabStyle}
							onTabPress={this.handleIndexChange}
						/>
						{/** select stock starts */}
						<View style={styles.timeframeContainer}>
							<Image
								source={require('@assets/Images/Home/stock.png')}
								style={[styles.iconStyle, { top: '80%' }]}
							/>
							<TouchableOpacity
								onPress={() => this.setState({ showSearchStockModal: true })}
								style={styles.timeframeButton}
							>
								<Text style={styles.timeframeButtonText}>
									{stockValue ? stockValue : 'Search Stock'}
								</Text>
							</TouchableOpacity>
						</View>

						{/** invested amount starts */}
						<View style={styles.investAmountContainer}>
							<Image source={require('@assets/Images/Home/rupee.png')} style={styles.iconStyle} />
							<TextInput
								placeholder="Investment Amount"
								value={invstAmt}
								placeholderTextColor={'#000000'}
								style={[styles.investInput]}
								keyboardType="number-pad"
								returnKeyType="done"
								onChangeText={d => {
									this.handleInvestement(d);
								}}
							/>
							{invstAmt ? (
								<TouchableOpacity
									onPress={() => this.handleInvestementAmount()}
									style={styles.clearButton}
								>
									<Image source={require('@assets/Images/error.png')} style={styles.clearIcon} />
								</TouchableOpacity>
							) : null}
						</View>

						{/** time frame starts */}
						<View style={styles.timeframeContainer}>
							<Image
								source={require('@assets/Images/Home/watch.png')}
								style={[styles.iconStyle, { top: '80%' }]}
							/>
							<TouchableOpacity
								onPress={() => this.setState({ showDurationList: true })}
								style={styles.timeframeButton}
							>
								<Text style={styles.timeframeButtonText}>
									{selectedDurationText ? selectedDurationText : 'Time Period'}
								</Text>
							</TouchableOpacity>
						</View>
						<TouchableOpacity onPress={this.handleCalculate} style={styles.btn}>
							<Text style={styles.btnText}>Know Your Return</Text>
						</TouchableOpacity>
						<Text style={styles.noteText}>
							<Text style={styles.boldFont}>Note: </Text>
							<Text style={styles.regularFont}>
								It will also show you stocks which delivered max return in the selected time period.
							</Text>
						</Text>
					</ScrollView>
				</View>
				<AlertComponent
					showModal={showError}
					title={errorTitle}
					description={errorMessage}
					onClose={() => this.onCloseErrorBox()}
				/>
				<DurationListComponent
					showModal={showDurationList}
					handleTimePeriod={(data, i) => this.handleTimePeriod(data, i)}
					onClose={() => this.onCloseModel()}
				/>
				<Modal
					visible={showSearchStockModal}
					animated
					animationType="slide"
					onDismiss={() => this.setState({ showSearchStockModal: false })}
				>
					<View style={styles.container}>
						<View style={styles.modalHeader} />
						<TouchableOpacity
							onPress={() => this.setState({ showSearchStockModal: false })}
							style={styles.modalCloseButton}
						>
							<Image source={require('@assets/Images/backArrow.png')} style={styles.modalCloseIcon} />
						</TouchableOpacity>
						<View style={styles.searchBar}>
							<TextInput
								ref={inp => (this.searchBar = inp)}
								style={styles.searchInputStyle}
								placeholder="Search For Stock Here"
								placeholderTextColor={'#000000'}
								value={stockValue}
								onChangeText={txt => this.handleStoc(txt)}
							/>
							{stockValue ? (
								<TouchableOpacity
									onPress={() =>
										this.setState({ stockValue: '', stockData: [], passedData: undefined })
									}
									style={styles.clearButton}
								>
									<Image source={require('@assets/Images/error.png')} style={styles.clearIcon} />
								</TouchableOpacity>
							) : (
									<TouchableOpacity onPress={() => this.searchBar.focus()} style={styles.clearButton}>
										<Image source={require('@assets/Images/search.png')} style={styles.clearIcon} />
									</TouchableOpacity>
								)}
						</View>
						{stockData.length > 0 ? (
							<FlatList
								style={styles.listStyle}
								contentContainerStyle={{ paddingBottom: 20 }}
								data={stockData}
								keyExtractor={(item, index) => index.toString()}
								renderItem={({ item }) => {
									return (
										<TouchableOpacity
											activeOpacity={0.9}
											onPress={() => {
												this.setState({
													stockValue: item.company_name,
													passedData: item,
													showSearchStockModal: false,
												});
											}}
											style={[styles.renderSearchRow]}
										>
											<Text style={styles.listLabelStyle}>{item.company_name}</Text>
										</TouchableOpacity>
									);
								}}
							/>
						) : null}
					</View>
				</Modal>
				{showAd && (
					<Banner
						unitId={unitId}
						size={'SMART_BANNER'}
						request={request.build()}
						onAdLoaded={() => { }}
						onAdFailedToLoad={abc => { }}
					/>
				)}
			</View>
		);
	}
}

const mapStateToProps = state => {
	return {
		data: state.dataReducer,
	};
};

const mapDispatchToProps = dispatch => {
	return {
		updateStockShowAdCount: () => dispatch(dataActions.updateStockShowAdCount()),
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Home);
