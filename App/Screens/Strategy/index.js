import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
} from 'react-native';
import Header from '@component/Header';
import { Fonts } from '@theme/Fonts';
import { Colors } from '@theme/Colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.listPageBackgroundColor,
  },
  scrollViewContainer: {
    paddingTop: 10,
    paddingHorizontal: 15,
  },
  textStyle: {
    fontSize: 16,
    fontFamily: Fonts.RegularFont,
    color: '#000',
    textAlign: 'justify'
  },
});

export default class Strategy extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { componentId } = this.props;
    return (
      <View style={styles.container}>
        <Header
          componentId={componentId}
          showBack
          title="Strategy"
        />
        <ScrollView contentContainerStyle={styles.scrollViewContainer}>
          <Text style={styles.textStyle}>
            1. Find the stock(s)  that was giving negative returns from the past 1,3,6 or 12 month  time frame and check if that stock(s) is giving positive return in the time frame of week 1 and week 2, as this stocks were corrected from some time and started giving return in recent time frame, it may have an higher upside potential.{'\n\n'}
            2. Invest in the stock which gave good return in one week, as they may continue for the second week also.{'\n\n'}
            3. Checkout the sector in which majority stocks are gainers; invest in good stocks from that sector.{'\n\n'}
            4. Checkout the sector in which majority stocks are losers; short weaker stocks from that sector.
          </Text>
        </ScrollView>
      </View>
    );
  }
}