import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
} from 'react-native';
import Header from '@component/Header';
import { Fonts } from '@theme/Fonts';
import { Colors } from '@theme/Colors';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.listPageBackgroundColor,
  },
  scrollViewContainer: {
    paddingTop: 10,
    paddingHorizontal: 15,
  },
  textStyle: {
    fontSize: 16,
    fontFamily: Fonts.RegularFont,
    color: '#000',
    textAlign: 'justify'
  },
});

export default class Disclaimer extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { componentId } = this.props;
    return (
      <View style={styles.container}>
        <Header
          componentId={componentId}
          showBack
          title="Disclaimer"
        />
        <ScrollView contentContainerStyle={styles.scrollViewContainer}>
          <Text style={styles.textStyle}>
            We try to provide the most accurate information to our users, but before making any decision, please contact your financial advisers. Any losses made in the market based on the data from this app are your responsibility and app does not take any responsibility for your profit or losses made by you.{'\n\n'}Only consider the data as help in making a sound decision while purchasing or selling any stock(s).
          </Text>
        </ScrollView>
      </View>
    );
  }
}