import { StyleSheet, Dimensions } from 'react-native';
import { Fonts } from '@theme/Fonts';

const { width } = Dimensions.get('window');

export default StyleSheet.create({
  cardContainer: {
    marginHorizontal: 15,
    minHeight: 70,
    borderRadius: 5,
    padding: 10,
    marginBottom: 20,
    backgroundColor: '#fff',
    shadowColor: '#000',
    shadowOffset: { width: 1, height: 1 },
    shadowOpacity: 0.2,
    shadowRadius: 2,
    elevation: 3,
    // borderWidth: StyleSheet.hairlineWidth,
  },
  nameContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginBottom: 20,
  },
  barStyle: {
    height: 5,
    overflow: 'visible',
    width: width - 60,
    backgroundColor: '#d3d3d3',
    justifyContent: 'center',
    borderRadius: 5,
  },
  gradientViewStyle: {
    width: '100%',
    height: '100%',
    borderRadius: 5,
  },
  companyNameLabel: {
    color: '#000',
    fontSize: 20,
    fontFamily: Fonts.BoldFont,
    flex: 1,
  },
  perLabel: {
    color: '#000',
    fontSize: 15,
    fontFamily: Fonts.BoldFont,
  },
});
