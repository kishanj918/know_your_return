import React, { Component } from 'react';
import {
  View,
  Text,
  Animated,
  Dimensions,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import styles from './style';
import { Colors } from '@theme/Colors';

export default class ProgressCard extends Component {
  constructor(props) {
    super(props);
    this.state = {
      animated: new Animated.Value(0),
    };
  }

  componentDidMount() {
    const { timeFrame, data } = this.props;
    Animated.timing(this.state.animated, {
      toValue: this.calculateBarWidth(data[timeFrame]),
      duration: 1000
    }).start();
  }

  calculateBarWidth(per) {
    let val = Math.abs(per);
    let percentage = parseFloat((100 * val) / 100).toFixed(2);
    if (percentage > 100) {
      percentage = parseFloat(100).toFixed(2);
    }
    let width = Dimensions.get('window').width - 60;
    const countWidth = (parseFloat(width).toFixed(2) * percentage) / 100;
    return countWidth;
  }

  render() {
    const { data, timeFrame } = this.props;
    const profitLossProgressColor = Number(data[timeFrame]) <= 0 ? Colors.progressDecrease : Colors.progressIncrease;
    const profitLossTextColor = Number(data[timeFrame]) <= 0 ? Colors.progressLoss : Colors.progressProfit;
    return (
      <View style={styles.cardContainer}>
        <View style={styles.nameContainer}>
          <Text style={styles.companyNameLabel}>{data.company_name}</Text>
          <Text style={[styles.perLabel, { color: profitLossTextColor }]}>{data[timeFrame]}%</Text>
        </View>
        <View style={styles.barStyle}>
          <Animated.View
            style={{
              height: 7,
              width: this.state.animated,
            }}
          >
            <LinearGradient
              style={styles.gradientViewStyle}
              colors={profitLossProgressColor}
              start={{ x: 0, y: 1 }}
              end={{ x: 1, y: 0 }}
            />
          </Animated.View>
        </View>
      </View>
    );
  }
}