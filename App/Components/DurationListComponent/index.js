import React, { Component } from 'react';
import {
  Text,
  TouchableOpacity,
  StyleSheet,
  FlatList,
  Dimensions,
} from 'react-native';
import { timeDurationData } from '@config/config';
import { Colors } from '@theme/Colors';
import { Fonts } from '@theme/Fonts';
import Overlay from 'react-native-modal-overlay';

const { width } = Dimensions.get('screen');
const styles = StyleSheet.create({
  containerStyle: {
    backgroundColor: 'rgba(0, 0, 0, 0.78)',
  },
  childrenWrapperStyle: {
    backgroundColor: '#fff',
    borderRadius: 20,
    maxHeight: 300,
    paddingHorizontal: 0
  },
  titleText: {
    textAlign: 'center',
    fontSize: 20,
    fontFamily: Fonts.BoldFont,
    color: '#000',
    paddingVertical: 5,
  },
  buttonStyle: {
    height: 40,
    width: width - 90,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.btnColor,
    marginTop: 10,
    alignSelf: 'center',
    borderRadius: 7,
  },
  buttonTextStyle: {
    textAlign: 'center',
    fontSize: 15,
    fontFamily: Fonts.BoldFont,
    color: '#fff',
  },
});

export default class DurationListComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { showModal = false, handleTimePeriod, onClose } = this.props;
    return (
      <Overlay
        visible={showModal}
        closeOnTouchOutside={true}
        animationType="zoomIn"
        containerStyle={styles.containerStyle}
        childrenWrapperStyle={styles.childrenWrapperStyle}
        animationDuration={500}
        onClose={onClose}
      >
        <Text style={styles.titleText}>Select Time Period</Text>
        <FlatList
          data={timeDurationData}
          keyExtractor={(item, index) => index.toString()}
          renderItem={({ item, index }) => {
            return (
              <TouchableOpacity
                activeOpacity={0.9}
                onPress={() => handleTimePeriod(item, index)}
                style={styles.buttonStyle}
              >
                <Text style={styles.buttonTextStyle}>{item.value}</Text>
              </TouchableOpacity>
            );
          }}
        />
      </Overlay>
    );
  }
}
