import React from 'react';
import {
  Text,
  TouchableOpacity,
  StyleSheet,
} from 'react-native';
import Overlay from 'react-native-modal-overlay';
import { Colors } from '@theme/Colors';
import { Fonts } from '@theme/Fonts';

const styles = StyleSheet.create({
  containerStyle: {
    backgroundColor: 'rgba(0, 0, 0, 0.78)',
  },
  childrenWrapperStyle: {
    backgroundColor: '#fff',
    borderRadius: 20
  },
  titleText: {
    textAlign: 'center',
    fontSize: 20,
    fontFamily: Fonts.BoldFont,
    color: '#000',
    paddingVertical: 5,
  },
  messageText: {
    textAlign: 'center',
    fontSize: 15,
    fontFamily: Fonts.BoldFont,
    color: '#000',
    paddingVertical: 5,
  },
  buttonStyle: {
    height: 40,
    width: '90%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: Colors.btnColor,
    marginTop: 20,
    alignSelf: 'center',
    borderRadius: 7,
  },
  buttonTextStyle: {
    textAlign: 'center',
    fontSize: 15,
    fontFamily: Fonts.BoldFont,
    color: '#fff',
  },
});

const AlertComponent = (props) => {
  const { showModal = false, title = undefined, description = undefined, onClose } = props;
  return (
    <Overlay
      visible={showModal}
      closeOnTouchOutside={true}
      animationType="zoomIn"
      containerStyle={styles.containerStyle}
      childrenWrapperStyle={styles.childrenWrapperStyle}
      animationDuration={500}
      onClose={onClose}
    >
      {title && <Text style={styles.titleText}>{title}</Text>}
      {description && <Text style={styles.messageText}>{description}</Text>}
      <TouchableOpacity
        onPress={onClose}
        style={styles.buttonStyle}
      >
        <Text style={styles.buttonTextStyle}>Ok</Text>
      </TouchableOpacity>
    </Overlay>
  );
}

export default AlertComponent;