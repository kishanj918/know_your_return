import { StyleSheet, Platform } from 'react-native';
import { ifIphoneX } from 'react-native-iphone-x-helper';
import { Colors } from '@theme/Colors';
import { Fonts } from '@theme/Fonts';

export default StyleSheet.create({
  main: {
    ...ifIphoneX({
      paddingTop: 50
    }, {
        paddingTop: Platform.OS == 'ios' ? 25 : 10
      }),
    paddingLeft: 10,
    paddingBottom: 10,
    // alignItems: 'center',
    backgroundColor: '#1a1a1a'
    // justifyContent: 'space-between',
  },
  mainContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
  },
  container: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    textAlign: 'center',
    fontSize: 20,
    color: '#0235ff',
    fontFamily: Fonts.BoldFont,
    textAlignVertical: 'center',
  },
  position: {
    position: 'absolute',
    // top: 5,
    left: 0,
  },
  backIcon: {
    height: 25,
    width: 25,
    tintColor: Colors.backColor,
    resizeMode: 'contain'
  },
  iconsView: {
    flexDirection: 'row',
    alignItems: 'center',
    ...ifIphoneX({
      position: 'absolute',
      right: 10,
      top: 55,
    }, {
        position: 'absolute',
        right: 10,
        top: 25,
      }),
  }
})