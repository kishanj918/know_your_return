import React from 'react';
import {
  View,
  Text,
  SafeAreaView,
  TouchableOpacity,
  Image,
} from 'react-native';
import { Navigation } from 'react-native-navigation';
import { Colors } from '@theme/Colors';
import theme from '@theme/theme';


import styles from './style';

const HomeHeader = (props) => {
  const {
    title,
    showBack,
    componentId,
    showReturn,
    returnValue,
    onSearchPressed
  } = props;

  return (
    <View style={styles.main}>
      <View style={[styles.mainContainer]}>
        <View style={[styles.container, { alignItems: 'center' }]}>
          <Text style={[styles.title, { color: '#fff' }]}>
            {title}
          </Text>
        </View>
      </View>
      <View style={[styles.iconsView]}>
        <TouchableOpacity onPress={onSearchPressed}>
          <Image
            source={require('@assets/Images/search.png')}
            style={[styles.backIcon, { marginHorizontal: 20 }]}
          />
        </TouchableOpacity>
        {/* <TouchableOpacity
          onPress={() => {
            Navigation.push(componentId, {
              component: {
                name: 'app.Sectors',
                passProps: {
                  text: 'Pushed screen'
                },
                options: {
                  topBar: {
                    title: {
                      text: 'Pushed screen title'
                    }
                  }
                }
              }
            });
          }}
        >
          <Image tintColor="#000" source={require('@assets/Images/dashboard.png')} style={styles.backIcon} />
        </TouchableOpacity> */}
      </View>
    </View>
  );
}

export default HomeHeader;

