import React from 'react';
import {
  View,
  Text,
  SafeAreaView,
  TouchableOpacity,
  Image,
} from 'react-native';
import { Navigation } from 'react-native-navigation';
import LinearGradient from 'react-native-linear-gradient';
import { Colors } from '@theme/Colors';
import theme from '@theme/theme';


import styles from './style';

const Header = (props) => {
  const {
    title,
    showBack,
    componentId,
    showReturn,
    returnValue,
  } = props;

  return (
    <LinearGradient
      start={{ x: 0, y: 1 }}
      end={{ x: 1, y: 0 }}
      colors={Colors.headerColors}
      style={styles.main}
    >
      <View style={styles.mainContainer}>
        {
          showBack
            ? (
              <TouchableOpacity style={styles.position} onPress={() => { Navigation.pop(componentId) }}>
                <Image source={require('@assets/Images/backArrow.png')} style={styles.backIcon} />
              </TouchableOpacity>
            ) : null
        }
        <View style={[styles.container]}>
          <Text style={[styles.title, { color: '#fff' }]}>
            {title}
          </Text>
        </View>
      </View>
      {
        showReturn
          ? (
            <View style={[styles.container, { paddingTop: 30 }]} >
              <Text style={[theme.com, { color: '#fff', textAlign: 'center' }]}>
                Investment Return {"\n"} Rs.{returnValue}
              </Text>
            </View >
          ) : null
      }
    </LinearGradient >
  );
}

export default Header;

