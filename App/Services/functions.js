import NetInfo from '@react-native-community/netinfo';

export const getNetConnectionInfo = () => {
  return new Promise((resolve, reject) => {
    NetInfo.getConnectionInfo().then((connectionInfo) => {
      resolve(connectionInfo);
    });
  })
}