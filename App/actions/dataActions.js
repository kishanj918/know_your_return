import * as types from './actionTypes';

export const updateStockShowAdCount = () => ({
  type: types.UPDATE_SEARCH_STOCKS_DISPLAY_AD_COUNT,
});

export const updateSectorShowAdCount = () => ({
  type: types.UPDATE_SEARCH_SECTORS_DISPLAY_AD_COUNT,
});