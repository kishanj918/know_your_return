export const BASE_URL = 'http://stock.apitestdomain.xyz/api/';

export const timeDurationData = [
  {
    name: '1_Week',
    value: '1 Week',
  },
  {
    name: '2_Week',
    value: '2 Weeks',
  },
  {
    name: '1_Month',
    value: '1 Month',
  },
  {
    name: '3_Month',
    value: '3 Months',
  },
  {
    name: '6_Month',
    value: '6 Months',
  },
  {
    name: '9_Month',
    value: '9 Months',
  },
  {
    name: '1_Year',
    value: '1 Year',
  },
];
