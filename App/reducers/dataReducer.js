import * as types from '../actions/actionTypes';

const initialState = {
  knowYourReturnAdCount: 0,
  searchSectorAdCount: 0,
};

export default function root(state = initialState, action = {}) {
  switch (action.type) {
    case types.UPDATE_SEARCH_STOCKS_DISPLAY_AD_COUNT:
      return {
        ...state,
        knowYourReturnAdCount: state.knowYourReturnAdCount += 1,
      }
    case types.UPDATE_SEARCH_SECTORS_DISPLAY_AD_COUNT:
      return {
        ...state,
        searchSectorAdCount: state.searchSectorAdCount += 1,
      };
    default:
      return state;
  }
}