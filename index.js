/**
 * @format
 * @lint-ignore-every XPLATJSCOPYRIGHT1
 */

import { AppRegistry, YellowBox } from 'react-native';
import App from './App/Navigation/index'
import { Sentry } from 'react-native-sentry';
Sentry.config('https://e4bc66a3b3bc496480ef92689b1059dd@sentry.io/1506742').install();

YellowBox.ignoreWarnings(['RNCNetInfo', 'Remote debugger', 'Require cycle'])

export default new App()
